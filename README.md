# Artefakt technical test

# Introduction

Unfortunately not as requested, this repo is merely to illustrate some of the kubernetes concepts I have previously used.

# Structure

deployments - micro-service deployment files
ingress - communication to the outside world from the cluster
kube - kubectl configuration file(s) (redacted)
secrets - secrets for providing TLS on endpoints (redacted)
services - discovery of pods for inter-cmmunication of micro-services or between the ingress and the pod

# Pre-requisites

None

# Running

None
